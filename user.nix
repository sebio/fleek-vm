{ pkgs, misc, ... }: {
  # FEEL FREE TO EDIT: This file is NOT managed by fleek.

  # ENV VARS
  programs.bash.sessionVariables = {
    CVS_RSH = "ssh";
    PAGER = "less -R -X -F";
    EDITOR = "micro";
    ## notes-cli
    NOTES_CLI_HOME = "$HOME/ghq/gitlab.com/s-schmeier/notes-cli";
    NOTES_CLI_EDITOR = "micro";
    NOTES_CLI_PAGER = "less -R -F -X";
    ## ghq root for repos
    GHQ_ROOT = "$HOME/ghq";
    ## micro editor color
    MICRO_TRUECOLOR = 1;
    ## cheat
    CHEAT_USE_FZF = true;
    ## SOPS
    SOPS_AGE_KEY_FILE = "$HOME/key.txt";
  };


  # ADD CUSTOM DOTFILES
  home.file.".hushlogin".source = ./resources/dotfiles/dot_hushlogin;
  home.file.".wgetrc".source = ./resources/dotfiles/dot_wgetrc;
  home.file.".screenrc".source = ./resources/dotfiles/dot_screenrc;
  home.file.".curlrc".source = ./resources/dotfiles/dot_curlrc;
  home.file.".emacs".source = ./resources/dotfiles/dot_emacs;
  home.file.".vimrc".source = ./resources/dotfiles/dot_vimrc;
  home.file.".condarc".source = ./resources/dotfiles/dot_condarc;
  home.file.".config/cheat/conf.yml".source = ./resources/dotfiles/dot_config/cheat/conf.yml;

  # custom bash-completions
  home.file.".local/share/bash-completion/cheat.bash".source = ./resources/dotfiles/dot_local/share/bash-completion/cheat.bash;
  home.file.".local/share/bash-completion/ghq.bash".source = ./resources/dotfiles/dot_local/share/bash-completion/ghq.bash;
  home.file.".local/share/bash-completion/devbox.bash".source = ./resources/dotfiles/dot_local/share/bash-completion/devbox.bash;

  # BASH
  programs.bash = {
    enableCompletion = true;
    initExtra = ''
      # check if notes-cli is installed and run completions
      if command -v notes &> /dev/null; then
        eval "$(notes --completion-script-bash)"
      fi
      # source addtional bash completion scripts
      for i in ~/.local/share/bash-completion/*.bash; do source $i; done
    '';
  };

  # MICRO
  # could be managed by home manger then put micro in programs and not packaes
  #home.file.".config/micro/colorschemes/dracula.micro".source = ./resources/dotfiles/dot_config/micro/colorschemes/dracula.micro;
  home.file.".config/micro/settings.json".source = ./resources/dotfiles/dot_config/micro/settings.json;
  #programs.micro.settings = {
  #  colorscheme = "solarized";
  #  autosu = true;
  #  colorcolumn = 80;
  #};


  # GIT
  programs.git = {
    aliases = {
      co = "checkout";
      ci = "commit";
      st = "status -sb";
      br = "branch";
      hist = "log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short";
      ls = "ls-tree -r master --name-only";
      wdiff = "diff --word-diff=color --unified=1";

      # View abbreviated SHA, date, description, and history graph of the latest 20 commits
      l = "log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short -n 20";

      # View the current working tree status using the short format
      s = "status -s";

      # Switch to a branch, creating it if necessary
      go = "!f() { git checkout -b \"$1\" 2> /dev/null || git checkout \"$1\"; }; f";

      # Show verbose output about tags, branches or remotes
      tags = "tag -l";
      bra = "branch -a";
      remotes = "remote -v";
    };

    extraConfig = {
      core = {
        editor = "micro";
      };
    };

    attributes = [
      "*.docx diff=pandoc"
      "*.env filter=sops"
    ];
  }; # GIT


  # TMUX
  programs.tmux = {
    clock24 = true;
    prefix = "C-v";
    mouse = false;
    extraConfig = ''
      # split panes using | and -
      bind | split-window -h
      bind - split-window -v
      unbind '"'
      unbind %
      # switch panes using Alt-arrow without prefix
      bind -n M-Left select-pane -L
      bind -n M-Right select-pane -R
      bind -n M-Up select-pane -U
      bind -n M-Down select-pane -D
    '';
    plugins = with pkgs.tmuxPlugins; [
      #sensible  # on by default
      yank
    ];
  };

}
